import './App.css';

function App() {
  return (
    <div className="App">
      <header>
        <img src="https://upload.wikimedia.org/wikipedia/commons/5/52/VAMK_logo.png" alt="Logo"></img>
        <h1 style={{ fontSize: 50 }}><span>Antto Hiltunen</span></h1>
        <br></br>
        <p className='musta-boldi'><span>Opiskelija</span></p>
        <p className='musta-ei-boldi'><span>Tietotekniikan insinööri</span></p>
        <br></br>
        <p className='musta-boldi'><span>Student</span></p>
        <p className='musta-ei-boldi'><span>IT-engineering</span></p>
        <br></br>
        <h2><span>Wolffintie 30, FI-65200 VAASA, Finland</span></h2>
        <h2><span>antto.hiltunen@gmail.com</span></h2>
      </header>
    </div>
  );
}

export default App;
